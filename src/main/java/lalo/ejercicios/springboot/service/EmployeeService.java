/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lalo.ejercicios.springboot.service;

import lalo.ejercicios.springboot.model.Employee;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);
    
}
