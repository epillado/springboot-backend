package lalo.ejercicios.springboot.service.impl;

import lalo.ejercicios.springboot.model.Employee;
import lalo.ejercicios.springboot.repository.EmployeeRepository;
import lalo.ejercicios.springboot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    
    //@Autowired
    //private EmployeeRepository employeeRepository;

    // @Override Can be omitted because there is only one constructor in class configured as Spring bean
    public Employee saveEmployee(Employee employee) {
        //return employeeRepository.save(employee);
        return null;
    }
    
}
