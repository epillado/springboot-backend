/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lalo.ejercicios.springboot.repository;

import lalo.ejercicios.springboot.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author eduar
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
    
}
